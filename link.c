/********************************************************
Copyright (C), 2011 - 2015, Multicoreware Inc.
FileName: Link.c
Author:周旭东 Version : 1.0 Date: 2015.4.20
Description:对链表进行操作.链表中的节点中包含char* Key,void* value2个元素,可以 
Version: 1.0
Function List: // 主要函数及其功能
1.Link_t LinkTable_new(void);
作用是建立新的链表,如果内存不够就返回 NULL pointer,否则返回结构体指针

2.void LinkTable_free(Link_t oLink);
作用是释放链表由指针 oLink 指向的,链表所占用的内存。包括所以已经分配的结构体

3.int LinkTable_getLength(Link_t oLink);
返回链表中结点的个数

4.int Link_put(Link_t oLink,const char *pcKey,const void *pvValue);
如果在链表中不存在 pcKey,则将 pcKey 和 pvValue 插入链表并返回 1。如果没有内存空间或pvValue 已经存在于链表之中则返回 0

5.void *Link_replace(Link_t oLink,const char *pcKey,const void *pvValue)
在链表中查找 pcKey,如果找到则将其对应的旧值替换为 pvValue 并返回指向旧值的指针。如果找不到则返回 NULL pointer

6.int Link_constains(Link_t oLink, const char *pcKey)
在链表中查找 pcKey,如果找到则返回 1,否则返回 0。该操作不对链表做任何修改

7.void *Link_get(Link_t oLink, const char *pcKey);
在链表中查找 pcKey,如果找到则返回其对应的值,否则返回 NULL pointer。该操作不对链表做任何修改

8.void *Link_remove(Link_t oLink, const char *pcKey);
在链表中查找 pcKey,如果找到则删除 pcKey 和对应的值,并返回指向旧值的指针。否则返回NULL pointer。

History:
<author> <time> <version > <desc>
David 96/10/12 1.0 build this moudle
********************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "link.h"

/*************************************************
Function:
LinkTable_new
Description:
创建一个新链表 
Calls:
Called By:
Table Accessed: 
Table Updated: 
Input:
Output: 
Return: 
创建新链表的头节点指针
Others: 
*************************************************/
Link_t LinkTable_new(void)
{
    Link_t nLink = (Link_t)malloc(sizeof(struct LinkNode));
   
    if (nLink == NULL)
    {
		printf("创建链表失败,分配空间失败");
        return NULL;
    }    
    
    memset(nLink,0,sizeof(struct LinkNode));

    return nLink;
}

/*************************************************
Function:
LinkTable_free
Description:
释放链表空间 
Calls:
Called By:
Table Accessed: 
Table Updated: 
Input:
要释放的链表头结点指针
Output: 
Return: 
Others: 
*************************************************/
void LinkTable_free(Link_t oLink)
{
    if (oLink == NULL)
    {
		printf("传入的参数不允许为NULL\n");
	    return;
    }
    
    Link_t delLink = NULL;
    Link_t nextLink = oLink;

    do
    {
        delLink = nextLink;

        delLink->Key = NULL;
                    
        delLink->value = NULL;
                
        nextLink = delLink->Next;
        free(delLink);
        
    } while(nextLink != NULL);
	
	printf("成功释放链表内存\n");

}

/*************************************************
Function:
LinkTable_getLength
Description:
返回链表中结点的个数
Calls:
Called By:
Table Accessed:
Table Updated: 
Input:
要查询的链表头结点指针
Output: 
Return: 
链表中结点的个数
Others: 
*************************************************/
int LinkTable_getLength(Link_t oLink)
{
    int num = 0;
    if (oLink == NULL)
    {
		printf("传入的参数不允许为NULL\n");
        return 0;
    }

    Link_t nextLink = oLink;

    do
    {
        num ++;
 
        nextLink = nextLink->Next;

    } while(nextLink != NULL);
    
    return num;
}

/*************************************************
Function:
Link_put
Description:
如果在链表中不存在 pcKey,则将 pcKey 和 pvValue 插入链表并返回 1。如果没有内存空间或pvValue 已经存在于链表之中则返回 0
Calls:
Called By:
Table Accessed: 
Table Updated: 
Input:
oLink:查询的链表头指针,pcKey:要查询的Key指,pvValue:插入新元素的value值
Output: 
Return:
1:在链表中不存在 pcKey,则将 pcKey 和 pvValue 插入链表.0:没有内存空间或pvValue已经存在于链表之中.
Others: 
*************************************************/
int Link_put(Link_t oLink,const char* pcKey,const void* pvValue)
{
    if (oLink == NULL || pcKey == NULL|| pvValue == NULL)
    {
		printf("传入的参数不允许为NULL\n");
    	return 1;
    }

    Link_t fLink  = oLink;

    do
    {   
		if( fLink->Key != NULL)
		{
			if (strcmp(fLink->Key,pcKey) == 0)
			{
            	return 0;
       		}
		}
    	
		if (fLink->Next != NULL)
		{
     		fLink = fLink->Next;
		}

    }while(fLink->Next != NULL);

   	Link_t nLink = LinkTable_new();
    
    if (nLink == NULL)
    {
		printf("给新元素分配空间失败");
        return 0;
    }

    nLink->Key = (char*)pcKey;
    nLink->value = (char*)pvValue;
    fLink->Next = nLink;

    return 1;
}

/*************************************************
Function:
Link_replace
Description:
在链表中查找 pcKey,如果找到则将其对应的旧值替换为 pvValue 并返回指向旧值的指针。如果找不到则返回 NULL pointer
Calls:
Called By:
Table Accessed: 
Table Updated: 
Input:
oLink:查询链表头指针,pcKey:要查询的Key值,pvValue:要替换的value值
Output: 
Return:
NULL:根据pcKey没有在链表中查询到对应的元素.其他:根据pcKey查询出了对应节点并把节点值替换为pvValue.
Others: 
*************************************************/
void* Link_replace(Link_t oLink, const char* pcKey,const char* pvValue)
{
	if (oLink == NULL || pcKey == NULL|| pvValue == NULL)
    {
        printf("传入的参数不允许为NULL\n");
        return NULL;
    }

    Link_t fLink  = oLink;

    do
    {
		if (fLink->Key != NULL)
		{
 	       	if (strcmp(fLink->Key,pcKey) == 0)
    	    {
           		fLink->value =(char*)pvValue;
            	return fLink;
        	}
		}
        fLink = fLink->Next;

    }while(fLink != NULL);

    return NULL;
}

/*************************************************
Function:
Link_constations
Description:
在链表中查找 pcKey,如果找到则返回 1,否则返回 0。该操作不对链表做任何修改
Calls:
Called By:
Table Accessed: 
Table Updated: 
Input:
oLink:查询链表头指针,pcKey:要查询的Key值.
Output: 
Return:
1:在链表中查询到pckey对应的结点,0在链表中没有查询到pcKey对应的节点
Others: 
*************************************************/
int Link_constains(Link_t oLink,const char* pcKey)
{
    if (oLink == NULL || pcKey == NULL)
    {
        printf("传入的参数不允许为NULL\n");
        return 0;
    }

    Link_t fLink  = oLink;

    do
    {
		if(fLink->Key != NULL)
		{
        	if (strcmp(fLink->Key,pcKey) == 0)
        	{
            	return 1;
        	}
		}
        fLink = fLink->Next;

    }while(fLink != NULL);

    return 0;
}

/*************************************************
Function:
Link_get
Description:
在链表中查找 pcKey,如果找到则返回其对应的值,否则返回 NULL pointer。该操作不对链表做任何修改
Calls:
Called By:
Table Accessed: 
Table Updated: 
Input:
oLink:查询链表头指针,pcKey:要查询的Key值.
Output: 
Return:
NULL:在链表中没有查询到pcKey的结点,其他:在链表中查询到pcKey对应的结点并返回Value的值.
Others: 
*************************************************/
void* Link_get(Link_t oLink,const char* pcKey)
{
   	if (oLink == NULL || pcKey == NULL)
    {
        printf("传入的参数不允许为NULL\n");
        return NULL;
    }
	
	Link_t fLink  = oLink;

    do
    {	if(fLink->Key != NULL)
		{
        	if (strcmp(fLink->Key,pcKey) == 0)
        	{
            	return fLink->value;
        	}
		}
       	fLink = fLink->Next;

    }while(fLink != NULL);

    return NULL;
}

/*************************************************
Function:
Link_remove
Description:
在链表中查找 pcKey,如果找到则返回其对应的值,否则返回 NULL pointer。该操>作不对链表做任何修改
Calls:
Called By:
Table Accessed: 
Table Updated: 
Input:
oLink:查询链表头指针,pcKey:要查询的Key值.
Output: 
Return:
NULL:在链表中没有查询到pcKey的结点,其他:在链表中查询到pcKey对应的结点并释放结点的内存,返回节点的旧指针
Others: 
*************************************************/
void* Link_remove(Link_t oLink,const char* pcKey)
{
    if (oLink == NULL || pcKey == NULL)
    {
        printf("传入的参数不允许为NULL\n");
        return NULL;
    }
    
	Link_t fLink = oLink;
	Link_t pLink = oLink;
	Link_t rLink = NULL;
 
    do
    {
		if (fLink->Key != NULL)
		{
        	if (strcmp(fLink->Key,pcKey) == 0)
        	{
           		fLink->Key = NULL;
            	fLink->value = NULL;
				pLink->Next = fLink->Next;
				rLink = fLink;
				free(fLink);
            	return rLink;
        	}
		}
		pLink = fLink;
        fLink = fLink->Next;

    }while(fLink != NULL);

    return NULL;
}
