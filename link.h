#include <stdlib.h>

typedef struct LinkNode
{
    char* Key;
    void* value;
    struct LinkNode* Next;
} *Link_t;

Link_t LinkTable_new(void);

void LinkTable_free(Link_t oLink);

int LinkTable_getLength(Link_t oLink);

int Link_put(Link_t oLink,const char* pcKey,const void* pvValue);

void* Link_replace(Link_t oLink, const char* pcKey,const char* pvValue);

int Link_constains(Link_t oLink,const char* pcKey);

void* Link_get(Link_t oLink,const char* pcKey);

void* Link_remove(Link_t oLink,const char* pcKey);



