#include <stdlib.h>
#include <stdio.h>
#include "link.h"

void PrintLink(Link_t oLink);

Link_t TestLinkTable_new(void)
{
	printf("开始单元测试LinkTable_new函数\n");

    Link_t link = LinkTable_new();
    
	if (link != NULL)
	{    
		printf("创建新的链表成功\n");
	}
    else
    {
        printf("创建新的链表失败\n");
 		return NULL;
    }

    link->Key = "TestKey";
    link->value = "TestValue";
	
	printf("结束单元测试LinkTable_new函数\n\n");

    return link;
}

void TestLinkTable_free(Link_t oLink)
{
	printf("开始单元测试LinkTable_free函数\n");

	LinkTable_free(oLink);	

	LinkTable_free(NULL);
	
	printf("结束单元测试LinkTable_free函数\n\n");
}

void TestLinkTable_getLength()
{
	printf("开始单元测试LinkTable_getLength函数\n");
	
	Link_t link = LinkTable_new();

	int num = LinkTable_getLength(link);

	PrintLink(link);

	printf("链表中有元素%d\n",num);

	
	printf("添加Key:1 Value:A1的元素\n");
    Link_put(link,"1","A1");

    printf("添加Key:2 Value:A2的元素\n");
    Link_put(link,"2","A2");

	num = LinkTable_getLength(link);

	PrintLink(link);

    printf("链表中有元素%d\n",num);

	LinkTable_free(link);

	printf("结束单元测试LinkTable_getLength函数\n\n");
}

void TestLink_put()
{
	int flag = 0;

	Link_t link = LinkTable_new();
	
	printf("开始单元测试Link_put函数\n");

	printf("添加Key:1 Value:A1的元素\n");
	flag = Link_put(link,"1","A1");
	printf("函数返回值为%d\n",flag);

	printf("添加Key:2 Value:A2的元素\n");	
	flag = Link_put(link,"2","A2");
	printf("函数返回值为%d\n",flag);

	int num = LinkTable_getLength(link);

	printf("链表中有元素%d\n",num);

	PrintLink(link);

	printf("添加Key=NULL,Value=NULL的元素\n");
	flag = Link_put(link,NULL,NULL);
	printf("函数返回值为%d\n",flag);

	num = LinkTable_getLength(link);
	printf("链表中有元素%d\n",num);
	
	PrintLink(link);

	LinkTable_free(link);

	printf("结束单元测试Link_put函数\n\n");
}

void TestLink_replace()
{
	Link_t link = LinkTable_new();
	
	printf("开始单元测试Link_replace函数\n");
    
	printf("添加Key:1 Value:A1的元素\n");
    Link_put(link,"1","A1");

    printf("添加Key:2 Value:A2的元素\n");
    Link_put(link,"2","A2");

	PrintLink(link);
	
	printf("替换Key=1的value=a1\n");
	Link_replace(link,"1","a1");

	PrintLink(link);
	
	printf("替换Key=3 value=A3\n");
	Link_replace(link,"3","A3");

	PrintLink(link);
	
	printf("替换Key=NULL Value=NULL\n");
	Link_replace(link,NULL,NULL);

	PrintLink(link);

	printf("替换Key=4 Value=NULL\n");
    Link_replace(link,"4",NULL);

    PrintLink(link);

    printf("替换Key=NULL,Value=4\n");
    Link_replace(link,NULL,"4");

    PrintLink(link);

	LinkTable_free(link);
	printf("结束单元测试Link_replace函数\n\n");
}

void TestLink_constatins()
{
    Link_t link = LinkTable_new();
	int flag = 0;

	printf("开始单元测试Link_constains函数\n");
	
	printf("添加Key:1 Value:A1的元素\n");
    Link_put(link,"1","A1");

    printf("添加Key:2 Value:A2的元素\n");
    Link_put(link,"2","A2");

    PrintLink(link);
	
	printf("查找Key=1的元素\n");
	flag = Link_constains(link,"1");
	printf("返回值为%d\n",flag);

	printf("查找Key=3的元素\n");
	flag = Link_constains(link,"3");
	printf("返回值为%d\n",flag);

	printf("查找Key=NULL的元素\n");
	flag = Link_constains(link,NULL);
	printf("返回值为%d\n",flag);

	LinkTable_free(link);

	printf("结束单元测试Link_constains函数\n\n");
}

void TestLink_get()
{
	void* rValue = NULL;
	printf("开始单元测试Link_get函数\n");

    Link_t link = LinkTable_new();
	
	printf("添加Key:1 Value:A1的元素\n");
    Link_put(link,"1","A1");
    
	printf("添加Key:2 Value:A2的元素\n");
   	Link_put(link,"2","A2");
    
	PrintLink(link);

	printf("查询key:1的节点\n");
	rValue = Link_get(link,"1");
	
	printf("查询出的节点value=%s\n",(char*)rValue);

	printf("查询Key:3的节点\n");
	rValue = Link_get(link,"3");
	
	printf("查询出的节点value=%s\n",(char*)rValue);	

	LinkTable_free(link);

	printf("结束单元测试Link_get函数\n\n");
}

void TestLink_remove()
{
	printf("开始单元测试Link_remove函数\n");
    Link_t link = LinkTable_new();

	printf("添加Key:1 Value:A1的元素\n");
    Link_put(link,"1","A1");

    printf("添加Key:2 Value:A2的元素\n");
    Link_put(link,"2","A2");

    PrintLink(link);

	printf("移除Key=1的元素\n");
	Link_t dLink = Link_remove(link,"1");
	printf("删除的元素地址:%p\n",dLink);
	
	PrintLink(link);

    printf("移除Key=NULL的元素\n");
    dLink = Link_remove(link,"NULL");
    printf("删除的元素地址:%p\n",dLink);

    PrintLink(link);
	
	LinkTable_free(link);
	printf("结束单元测试Link_remove函数\n\n");
}

void PrintLink(Link_t oLink)
{
	printf("开始输出链表内容\n");

	if (oLink == NULL)
	{
		return;
	}

	Link_t pLink = oLink;

	do
	{
		printf("链表元素地址:%p\n",pLink);

		if (pLink->Key != NULL)
		{
			printf("Key:%s\n",pLink->Key);
		}
		else
		{
			printf("Key:NULL\n");
		}

		if (pLink->value != NULL)
		{
			printf("Value:%s\n",(char*)pLink->value);
		}
		else
		{
			printf("Value:NULL\n");
		}

		pLink = pLink->Next;
	}
	while(pLink != NULL);	

	printf("结束输出链表内容\n");
}

int main(void)
{
	Link_t tstLink = NULL;

	tstLink = TestLinkTable_new(); 

	TestLinkTable_getLength();	
	
	TestLink_put();

	TestLinkTable_free(tstLink);

	TestLink_replace();
	
	TestLink_constatins();

	TestLink_get();

	TestLink_remove();
    return 0;
}
